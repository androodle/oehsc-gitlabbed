<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/*
 * @package    format
 * @subpackage tiles
 * @author     Greg Newton, Androgogic <greg.newton@androgogic.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @copyright  2014 Androgogic, Ltd.
 *
 * Renderer for component 'format_tiles'
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/course/format/renderer.php');

class format_tiles_renderer extends format_section_renderer_base {

    /**
     * Constructor method, calls the parent constructor
     *
     * @param moodle_page $page
     * @param string $target one of rendering target constants
     */
    public function __construct(moodle_page $page, $target) {
        parent::__construct($page, $target);

        // Since format_topics_renderer::section_edit_controls() only displays the 'Set current section' control when editing mode is on
        // we need to be sure that the link 'Turn editing mode on' is available for a user who does not have any other managing capability.
        $page->set_other_editing_capability('moodle/course:setcurrentsection');
    }

    /**
     * Generate the starting container html for a list of sections
     * @return string HTML to output.
     */
    protected function start_section_list() {
        return html_writer::start_tag('ul', array('class' => 'tileothertopics'));
    }

    /**
     * Generate the closing container html for a list of sections
     * @return string HTML to output.
     */
    protected function end_section_list() {
        return html_writer::end_tag('ul');
    }

    /**
     * Generate the title for this section page
     * @return string the page title
     */
    protected function page_title() {
        return null; // Unused at present, dummy method.
    }

    /**
     * Display the contents of a section.
     * @global object $PAGE standard Moodle page class
     * @param object $course The course
     * @param object $thissection The section
     * @param array $mods A list of available activity mods
     * @param array $modnames
     * @param array $modnamesused
     * @param boolean $canviewhidden Whether the user can view hidden sections
     */
    public function section_content($course, $thissection, $mods, $modnames, $modnamesused, $canviewhidden) {
        global $PAGE;

        $o = '';
        $coursecontext = context_course::instance($course->id);

        $streditsummary = get_string('editsummary');

        if (!$canviewhidden and !$thissection->visible) {   // Hidden for students
            $o .= get_string('notavailable');
        } else {
            #if (!is_null($thissection->name)) {
            #    $o .= $this->output->heading(format_string($thissection->name, true, array('context' => $coursecontext)), 3, 'sectionname');
            #}
            $o .= $this->output->heading(format_string(format_tiles_get_section_name($course, $thissection), true, array('context' => $coursecontext)), 3, 'sectionname');

            $o .= html_writer::start_tag('div', array('class' => 'summary'));
            if ($thissection->summary) {
                $summarytext = file_rewrite_pluginfile_urls($thissection->summary, 'pluginfile.php', $coursecontext->id, 'course', 'section', $thissection->id);
                $summaryformatoptions = new stdClass();
                $summaryformatoptions->noclean = true;
                $summaryformatoptions->overflowdiv = true;
                $o .= format_text($summarytext, $thissection->summaryformat, $summaryformatoptions);
            } else {
                $o .= '&nbsp;';
            }

            if ($PAGE->user_is_editing() && has_capability('moodle/course:update', context_course::instance($course->id))) {
                $o .= html_writer::start_tag('a', array('href' => 'editsection.php?id=' . $thissection->id, 'title' => $streditsummary));
                $o .= html_writer::empty_tag('img', array('src' => $this->output->pix_url('i/settings'), 'class' => 'iconsmall edit', 'alt' => $streditsummary));
                $o .= html_writer::end_tag('a');
                $o .= html_writer::empty_tag('br');
                $o .= html_writer::empty_tag('br');
            }
            $o .= html_writer::end_tag('div');

            $displayoptions = array('hidecompletion' => false);
            $courserenderer = $PAGE->get_renderer('core', 'course');
            $o .= $courserenderer->course_section_cm_list($course, $thissection, null, $displayoptions);
            $o .= html_writer::empty_tag('br');
            if ($PAGE->user_is_editing()) {
                $o .= $courserenderer->course_section_add_cm_control($course, $thissection->section, null,
                    array('inblock' => false));
            }
        }

        return $o;
    }

    /**
     * Displays the nav bar that is used at the top of the single topic view
     * @param object $course The current course
     * @param array $sections The sections of the course
     * @param int $displaysection The section currently being displayed
     * @param boolean $canviewhidden Can the current user view hidden sections?
     */
    public function nav_bar($course, $sections, $displaysection, $canviewhidden, $nav_pos = 'tilenav_top') {

        $o = '';

        $home_url = new moodle_url('/course/view.php', array('id' => $course->id, 'topic' => 0));
        $home_title = get_string('topicoutline');

        static $formatconfig = false;
        if ($formatconfig === false) {
            $formatconfig = get_config('format_tiles');
        }

        $o .= html_writer::start_tag('div', array('class' => 'tiletopicnav ' . $nav_pos));
        $o .= html_writer::start_tag('ul');

        // Home tab.
        $o .= html_writer::start_tag('li');
        $o .= html_writer::start_tag('a', array('href' => $home_url, 'class' => 'summary', 'title' => $home_title));
        $o .= html_writer::tag('span', 'Home');
        $o .= html_writer::end_tag('a');
        $o .= html_writer::end_tag('li');
        $o .= "\n"; # Maintain compatability with existing style sheets

        // Section tabs.
        $modinfo = get_fast_modinfo($course);
        foreach ($modinfo->get_section_info_all() as $section => $thissection) {

            // Don't show the summary section in the nav bar
            if ($section == 0) continue;

            // Extra orphaned sections
            if ($section > $course->numsections) {
                continue;
            }

            $classes = array();
            $is_current_section = (!empty($displaysection) and $displaysection == $section);
            if ($is_current_section) {
                $classes[] = "current";
            }

            if (course_get_format($course)->is_section_current($section)) {
                $classes[] = "currentsection";
            }

            $button_class = "";
            $button_class = implode(' ', $classes);

            $showsection = format_tiles_is_section_user_visible($course, $thissection, $canviewhidden);

            if ($showsection) {
                $url = new moodle_url('/course/view.php', array('id' => $course->id, 'topic' => $section));
                $title = format_tiles_get_section_name($course, $thissection);
                $content = $section;
                if ($formatconfig->allow_title_tabs && $course->titletabs) {
                    $content = $title;
                    if ($formatconfig->max_title_length and strlen($content) > $formatconfig->max_title_length) {
                        $content = mb_substr($content, 0, $formatconfig->max_title_length, "utf-8") . "...";
                    }
                }
                $o .= html_writer::start_tag('li');
                $o .= html_writer::start_tag('a', array('href' => $url, 'class' => $button_class, 'title' => $title));
                $o .= html_writer::tag('span', $content);
                $o .= html_writer::end_tag('a');
                $o .= html_writer::end_tag('li');
                $o .= "\n"; # Maintain compatability with existing style sheets
            }
        }

        // All sections tab.
        if ($formatconfig->allow_all_sections_view and $course->allsections) {
            $all_url = new moodle_url('/course/view.php', array('id' => $course->id, 'topic' => 'all'));

            $button_class = 'allsections';
            if (!empty($displaysection) and $displaysection == 'all') {
                $button_class = 'current allsections';
            }

            $o .= html_writer::start_tag('li');
            $o .= html_writer::start_tag('a', array('href' => $all_url, 'class' => $button_class, 'title' => get_string('allsections', 'format_tiles')));
            $o .= html_writer::tag('span', get_string('allsections', 'format_tiles'));
            $o .= html_writer::end_tag('a');
            $o .= html_writer::end_tag('li');
            $o .= "\n"; # Maintain compatability with existing style sheets
        }

        $o .= html_writer::end_tag('ul');
        $o .= html_writer::end_tag('div'); #class="tiletopicnav"

        return $o;
    }

    /**
     * Display a single tile
     * @param object $course The course (as an object)
     * @param int $section Current section number
     * @param object $thissection The section
     */
    public function tile($course, $section, $thissection) {
        global $DB;

        $o = '';
        $tile_class = "";
        if (course_get_format($course)->is_section_current($section)) {
            $tile_class = ' currentsection';
        }

        $section_name = format_tiles_get_section_name($course, $thissection);
        $url = new moodle_url('/course/view.php', array('id' => $course->id, 'topic' => $section));

        $coursecontext = context_course::instance($course->id);

        $bg_style = "";

        static $formatconfig = false;
        if ($formatconfig === false) {
            $formatconfig = get_config('format_tiles');
        }

        if ($formatconfig->allow_custom_tiles) {
            $tile_image = $DB->get_record('format_tiles_tile_image', array('courseid' => $course->id, 'sectionid' => $thissection->id));
            if ($tile_image) {
                $image_path = moodle_url::make_pluginfile_url(
                                $coursecontext->id, 'format_tiles', 'section', $thissection->id, '/',
                                $tile_image->filename);
                $bg_style = 'background-image: url(' . $image_path . ') !important;';
            }
        }

        $o .= html_writer::start_tag('li', array('class' => 'tile' . $tile_class, 'id' => 'tile' . $section));
        if (!empty($bg_style)) {
            $o .= html_writer::start_tag('a', array('href' => $url, 'style' => $bg_style));
        } else {
            $o .= html_writer::start_tag('a', array('href' => $url));
        }
        $o .= html_writer::tag('span', $section_name, array('class' => 'tiletitle'));
        $o .= html_writer::tag('span', $section, array('class' => 'tiletopic'));
        $o .= html_writer::end_tag('a');
        $o .= html_writer::end_tag('li');

        return $o;
    }

    /**
     * Display the content of a page in single section view.
     * @param object $course The current course
     * @param array $sections The sections of the course
     * @param int $displaysection The section currently being displayed
     * @param array $mods A list of available activity mods
     * @param array $modnames
     * @param array $modnamesused
     * @param boolean $canviewhidden Can the current user view hidden sections?
     */
    public function print_section_page($course, $sections, $displaysection, $mods, $modnames, $modnamesused, $canviewhidden) {

        $o = '';

        static $formatconfig = false;
        if ($formatconfig === false) {
            $formatconfig = get_config('format_tiles');
        }

        // Print the Your progress icon if the track completion is enabled
        $completioninfo = new completion_info($course);
        $o .= $completioninfo->display_help_icon();

        // The next two divs are wrappers intendend to facilitate CSS selectors in styles
        $o .= html_writer::start_tag('div', array('class' => 'tiles'));
        $o .= html_writer::start_tag('div', array('class' => 'tilesectionpage'));

        $o .= $this->nav_bar($course, $sections, $displaysection, $canviewhidden);

        if (!empty($displaysection)) {
            $thissection = format_tiles_get_or_create_section($course, $sections, $displaysection);
        }

        if (format_tiles_is_section_user_visible($course, $thissection, $canviewhidden)) {
            $o .= html_writer::start_tag('div', array('class' => 'tilecontentarea'));
            $o .= $this->section_content($course, $thissection, $mods, $modnames, $modnamesused, $canviewhidden);
            $o .= html_writer::end_tag('div');
        }

        if ($formatconfig->allow_bottom_tabs and $course->bottomtabs) {
            $o .= $this->nav_bar($course, $sections, $displaysection, $canviewhidden, 'tilenav_bottom');
        }

        $o .= html_writer::end_tag('div'); # class=tileoutlinepage/tilesectionpage
        $o .= html_writer::end_tag('div'); # class=tiles

        echo $o;
    }

    /**
     * Displays the course in all sections mode.
     * @param object $course The current course
     * @param array $sections The sections of the course
     * @param array $mods A list of available activity mods
     * @param array $modnames
     * @param array $modnamesused
     * @param boolean $canviewhidden Can the current user view hidden sections?
     */
    public function print_all_sections_page($course, $sections, $mods, $modnames, $modnamesused, $canviewhidden) {
        $o = '';

        $modinfo = get_fast_modinfo($course);
        $numsections = count($modinfo);

        $section = 0;
        $thissection = $sections[$section];
        unset($sections[0]);

        // Print the Your progress icon if the track completion is enabled
        $completioninfo = new completion_info($course);
        $o .= $completioninfo->display_help_icon();

        // The next two divs are wrappers intendend to facilitate CSS selectors in styles
        $o .= html_writer::start_tag('div', array('class' => 'tiles'));
        $o .= html_writer::start_tag('div', array('class' => 'tilesectionpage'));

        $o .= $this->nav_bar($course, $sections, 'all', $canviewhidden);

        // Frontpage outline area
        $o .= html_writer::start_tag('div', array('class' => 'tileoutlinearea'));
        $o .= $this->output->heading(get_string('topicoutline'), 2, 'headingblock header outline');
        $o .= html_writer::start_tag('div', array('class' => 'tileoutlinecontent no-overflow'));
        $o .= $this->section_content($course, $thissection, $mods, $modnames, $modnamesused, $canviewhidden);
        $o .= html_writer::end_tag('div'); # class="tileoutlinecontent
        $o .= html_writer::end_tag('div'); #id="tileoutlinearea"

    #    $strshowalltopics = get_string('showalltopics');

        // Output list of tiles
        $o .= html_writer::start_tag('ul', array('class' => 'tileoutlinetiles', 'id' => 'tileoutlinetiles'));
        $section = 1;

        $o .= html_writer::start_tag('div', array('class' => 'tilecontentarea'));
        $o .= html_writer::start_tag('ul', array('class' => 'tileothertopics'));
        foreach ($modinfo->get_section_info_all() as $section => $thissection) {

            // Don't show the home section as a tile
            if ($section == 0) continue;

            // Extra orphaned sections
            if ($section > $course->numsections) {
                continue;
            }

            $showsection = format_tiles_is_section_user_visible($course, $thissection, $canviewhidden);

            if ($showsection) {

                $sectionclass = "";
                if (course_get_format($course)->is_section_current($section)) {
                    $sectionclass = ' currentsection';
                }

                if (!$thissection->visible) {
                    $sectionclass .= ' hidden';
                }

                $o .= html_writer::start_tag('li', array('class' => 'tilecontentarea section main clearfix' . $sectionclass, 'id' => 'section-' . $section));
                $o .= $this->section_content($course, $thissection, $mods, $modnames, $modnamesused, $canviewhidden);
                $o .= html_writer::end_tag('li');
            }

            unset($sections[$section]);
        }
        $o .= html_writer::end_tag('ul');
        $o .= html_writer::end_tag('div');

        static $formatconfig = false;
        if ($formatconfig === false) {
            $formatconfig = get_config('format_tiles');
        }

        if ($formatconfig->allow_bottom_tabs and $course->bottomtabs) {
            $o .= $this->nav_bar($course, $sections, 'all', $canviewhidden, 'tilenav_bottom');
        }

        $o .= html_writer::end_tag('div'); # class=tileoutlinepage/tilesectionpage
        $o .= html_writer::end_tag('div'); # class=tiles

        echo $o;
    }

    /**
     * Display the summary page for the course.
     * @param object $course The course (as an object)
     * @param array $sections The sections (as objects) associated with the course.
     * @param array $mods A list of available activity mods
     * @param array $modnames
     * @param array $modnamesused
     * @param boolean $canviewhidden Whether the user can view hidden sections
     */
    public function print_course_summary_page($course, $sections, $mods, $modnames, $modnamesused, $canviewhidden) {
        $o = '';
        $modinfo = get_fast_modinfo($course);
        $numsections = count($modinfo);

        $section = 0;
        $thissection = $sections[$section];
        unset($sections[0]);

        // Print the Your progress icon if the track completion is enabled
        $completioninfo = new completion_info($course);
        $o .= $completioninfo->display_help_icon();

        // The next two divs are wrappers intendend to facilitate CSS selectors in styles
        $o .= html_writer::start_tag('div', array('class' => 'tiles'));
        $o .= html_writer::start_tag('div', array('class' => 'tileoutlinepage'));

        // Frontpage outline area
        $o .= html_writer::start_tag('div', array('class' => 'tileoutlinearea', 'id' => 'tileoutlinearea'));
        $o .= $this->output->heading(get_string('topicoutline'), 2, 'headingblock header outline');
        $o .= html_writer::start_tag('div', array('class' => 'tileoutlinecontent no-overflow'));
        $o .= $this->section_content($course, $thissection, $mods, $modnames, $modnamesused, $canviewhidden);
        $o .= html_writer::end_tag('div'); # class="tileoutlinecontent
        $o .= html_writer::end_tag('div'); #id="tileoutlinearea"

    #    $strshowalltopics = get_string('showalltopics');

        // Output list of tiles
        $o .= html_writer::start_tag('ul', array('class' => 'tileoutlinetiles', 'id' => 'tileoutlinetiles'));
        $section = 1;

        foreach ($modinfo->get_section_info_all() as $section => $thissection) {

            // Don't show the home section as a tile
            if ($section == 0) continue;

            // Extra orphaned sections
            if ($section > $course->numsections) {
                continue;
            }

            $showsection = format_tiles_is_section_user_visible($course, $thissection, $canviewhidden);

            if ($showsection) {
                $o .= $this->tile($course, $section, $thissection);
            }

            unset($sections[$section]);
            $section++;
        }

        static $formatconfig = false;
        if ($formatconfig === false) {
            $formatconfig = get_config('format_tiles');
        }

        if ($formatconfig->allow_all_sections_view and
            $formatconfig->allow_all_sections_tile and
            $course->allsections) {
            $all_url = new moodle_url('/course/view.php', array('id' => $course->id, 'topic' => 'all'));

            $button_class = "";
            if (!empty($displaysection) and $displaysection == 'all') {
                $button_class = 'current';
            }

            $o .= html_writer::start_tag('li', array('class' => 'tile allsections', 'id' => 'tileall'));
            $o .= html_writer::start_tag('a', array('href' => $all_url));
            $o .= html_writer::tag('span', get_string('allsections', 'format_tiles'), array('class' => 'tiletitle'));
            $o .= html_writer::tag('span', '', array('class' => 'tiletopic'));
            $o .= html_writer::end_tag('a');
            $o .= html_writer::end_tag('li');
        }
        $o .= html_writer::end_tag('ul'); #id="tileoutlinearea"

        $o .= html_writer::end_tag('div'); # class=tileoutlinepage/tilesectionpage
        $o .= html_writer::end_tag('div'); # class=tiles

        echo $o;
    }

    /**
     * Generate the edit controls of a section
     *
     * @param stdClass $course The course entry from DB
     * @param stdClass $section The course_section entry from DB
     * @param bool $onsectionpage true if being printed on a section page
     * @return array of links with edit controls
     */
    protected function section_edit_controls($course, $section, $onsectionpage = false) {
        global $PAGE;

        if (!$PAGE->user_is_editing()) {
            return array();
        }

        $coursecontext = context_course::instance($course->id);

        if ($onsectionpage) {
            $url = course_get_url($course, $section->section);
        } else {
            $url = course_get_url($course);
        }
        $url->param('sesskey', sesskey());

        $controls = array();
        if (has_capability('moodle/course:setcurrentsection', $coursecontext)) {
            if ($course->marker == $section->section) {  // Show the "light globe" on/off.
                $url->param('marker', 0);
                $controls[] = html_writer::link($url,
                                    html_writer::empty_tag('img', array('src' => $this->output->pix_url('i/marked'),
                                        'class' => 'icon ', 'alt' => get_string('markedthistopic'))),
                                    array('title' => get_string('markedthistopic'), 'class' => 'editing_highlight'));
            } else {
                $url->param('marker', $section->section);
                $controls[] = html_writer::link($url,
                                html_writer::empty_tag('img', array('src' => $this->output->pix_url('i/marker'),
                                    'class' => 'icon', 'alt' => get_string('markthistopic'))),
                                array('title' => get_string('markthistopic'), 'class' => 'editing_highlight'));
            }
        }

        return array_merge($controls, parent::section_edit_controls($course, $section, $onsectionpage));
    }

    /**
     * Display the course editting page.
     * @global object $PAGE standard Moodle page class
     * @param object $course The course
     * @param array $mods A list of available activity mods
     * @param array $modnames
     * @param array $modnamesused
     * @param boolean $canviewhidden Whether the user can view hidden sections
     */
    public function print_course_editing_page($course, $sections, $mods, $modnames, $modnamesused, $canviewhidden) {
        global $DB, $PAGE;

        $o = '';

        $modinfo = get_fast_modinfo($course);
        $numsections = count($modinfo);

    /// Now all the normal modules by topic
    /// Everything below uses "section" terminology - each "section" is a topic.

        $formatconfig = get_config('format_tiles');

        $context = context_course::instance($course->id);

        // Print the Your progress icon if the track completion is enabled
        $completioninfo = new completion_info($course);
        $o .= $completioninfo->display_help_icon();

        // The next two divs are wrappers intendend to facilitate CSS selectors in styles
        $o .= html_writer::start_tag('div', array('class' => 'tiles'));
        $o .= html_writer::start_tag('div', array('class' => 'tileoutlinepage'));

        //$o .= html_writer::start_tag('ul', array('class' => 'tileothertopics'));
        $o .= $this->start_section_list();

        foreach ($modinfo->get_section_info_all() as $section => $thissection) {

            // Extra orphaned sections
            if ($section > $course->numsections) {
                continue;
            }

            $showsection = format_tiles_is_section_user_visible($course, $thissection, $canviewhidden);

            if (!empty($displaysection) and $displaysection != $section) {  // Check this topic is visible
                if ($showsection) {
                    $sectionmenu[$section] = get_section_name($course, $thissection);
                }
                $section++;
                continue;
            }

            if ($showsection) {

                $currenttopic = ($course->marker == $section);

                if (!$thissection->visible) {
                    $sectionstyle = ' hidden';
                } else if ($currenttopic) {
                    $sectionstyle = ' current';
                } else {
                    $sectionstyle = '';
                }

                $o .= html_writer::start_tag('li', array('class' => 'tilecontentarea section main clearfix' . $sectionstyle, 'id' => 'section-' . $section));
//                $o .= html_writer::tag('div', $section, array('class' => 'left side'));
                $o .= html_writer::tag('div', '', array('class' => 'left side'));

                // Note, 'right side' is BEFORE content.
                $o .= html_writer::start_tag('div', array('class' => 'right side'));

                $controls = $this->section_edit_controls($course, $thissection, false);
                if (!empty($controls)) {
                    $o .= implode('<br />', $controls);
                }
                $o .= html_writer::end_tag('div'); //class="right side"

                $o .= html_writer::start_tag('div', array('class' => 'content'));
                $o .= $this->section_content($course, $thissection, $mods, $modnames, $modnamesused, $canviewhidden);

                $coursecontext = context_course::instance($course->id);

                if ($formatconfig->allow_custom_tiles && $section > 0 && has_capability('format/tiles:changetilebackground', $coursecontext)) {
                    $o .= html_writer::empty_tag('hr');
                    $o .= html_writer::tag('h4', get_string('tile_preview', 'format_tiles'));

                    $o .= html_writer::start_tag('ul', array('class' => 'tileoutlinetiles', 'id' => 'tileoutlinetiles'));

                    $o .= $this->tile($course, $section, $thissection);
                    #TODO: cache the following as it's also retreived in the previous function.
                    $tile_image = $DB->get_record('format_tiles_tile_image', array('courseid' => $course->id, 'sectionid' => $thissection->id));

                    $o .= html_writer::start_tag('li', array('class' => 'tilenotes'));
                    if ($tile_image) {
                        $user = $DB->get_record('user', array('id' => $tile_image->userid));
                        $date = date('Y-m-d', $tile_image->timemodified);
                        $o .= get_string('background_set', 'format_tiles', array('firstname' => $user->firstname, 'lastname' => $user->lastname, 'date' => $date));
                        $o .= html_writer::empty_tag('br');
                        $o .= html_writer::link(new moodle_url('/course/format/tiles/chooseimage.php', array('courseid' => $course->id, 'sectionid' => $thissection->id)), get_string('change_background', 'format_tiles'));
                        $o .= html_writer::empty_tag('br');
                        $o .= html_writer::link(new moodle_url('/course/format/tiles/removeimage.php', array('courseid' => $course->id, 'sectionid' => $thissection->id)), get_string('remove_background', 'format_tiles'));
                    } else {
                        $o .= get_string('background_not_set', 'format_tiles');
                        $o .= html_writer::empty_tag('br');
                        $o .= html_writer::link(new moodle_url('/course/format/tiles/chooseimage.php', array('courseid' => $course->id, 'sectionid' => $thissection->id)), get_string('set_background', 'format_tiles'));
                    }
                    $o .= html_writer::end_tag('li');
                    $o .= html_writer::end_tag('ul'); #id="tileoutlinearea"
                }

                $o .= html_writer::end_tag('div');
                $o .= html_writer::end_tag('li');
            }

            unset($sections[$section]);
            $section++;
        }

        if ($PAGE->user_is_editing() and has_capability('moodle/course:update', $context)) {
            // Print stealth sections if present.
            foreach ($modinfo->get_section_info_all() as $section => $thissection) {
                if ($section <= $course->numsections or empty($modinfo->sections[$section])) {
                    // this is not stealth section or it is empty
                    continue;
                }
                $o .= $this->stealth_section_header($section);
                $o .= $this->courserenderer->course_section_cm_list($course, $thissection, 0);
                $o .= $this->stealth_section_footer();
            }
        }

        #$o .= html_writer::end_tag('ul');
        $o .= $this->end_section_list();

        $o .= html_writer::end_tag('div'); # class=tileoutlinepage/tilesectionpage
        $o .= html_writer::end_tag('div'); # class=tiles

        echo $o;
    }

}