<?php

/**
 * Internal functions for the Androgogic Tile format.
 *
 * May contain some code derived from the Moodle topics format from Moodle 2.2,
 * originally developed by N.D.Freear@open.ac.uk and others and Copyright 2006
 * The Open University.
 *
 * @since 2.0
 * @package    format
 * @subpackage tiles
 * @copyright 2013 Androgogic
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// --------------------------------------------------------------------------
// This function is used to work around diffences between Moodle 2.2 and 2.3

/**
 * Returns the course section to display or 0 meaning show all sections. Returns 0 for guests.
 * It also sets the $USER->display cache to array($courseid=>return value)
 *
 * @param int $courseid The course id
 * @return int Course section to display, 0 means all
 */
function format_tiles_course_get_display($courseid) {
    global $USER, $DB;

    if (!isloggedin() or isguestuser()) {
        //do not get settings in db for guests
        return 0; //return the implicit setting
    }

    if (!isset($USER->display[$courseid])) {
        if (!$display = $DB->get_field('course_display', 'display', array('userid' => $USER->id, 'course' => $courseid))) {
            $display = 0; // all sections option is not stored in DB, this makes the table much smaller
        }
        //use display cache for one course only - we need to keep session small
        $USER->display = array($courseid => $display);
    }

    return $USER->display[$courseid];
}

// --------------------------------------------------------------------------

/**
 * Get a section from the list of sections in a course by number. If it doesn't
 * exist (Moodle takes a just-in-time approach), create it.
 * @global type $DB
 * @param object $course The course (as an object)
 * @param array $sections The sections (as objects) associated with the course.
 * @param type $section_num The number of the section
 * @return \stdClass The section (as an object)
 */
function format_tiles_get_or_create_section($course, $sections, $section_num) {
    global $DB;

    if (!empty($sections[$section_num])) {
        $thissection = $sections[$section_num];
    } else {
        $thissection = new stdClass;
        $thissection->course = $course->id;   // Create a new section structure
        $thissection->section = $section_num;
        $thissection->name = null;
        $thissection->summary = '';
        $thissection->summaryformat = FORMAT_HTML;
        $thissection->visible = 1;
        $thissection->id = $DB->insert_record('course_sections', $thissection);

        // Only needed for 2.3 — GJN
        $thissection->uservisible = true;
        $thissection->availableinfo = null;
        $thissection->showavailability = 0;

        $sections[$section_num] = $thissection;
    }
    return $thissection;
}

/**
 * Determine whether a section is visible to the current user.
 * @param object $thissection The section (as a data structure, not an id)
 * @param boolean $canviewhidden Whether the user can view hidden sections
 * @return boolean True if the section is visible
 */
function format_tiles_is_section_user_visible($course, $thissection, $canviewhidden) {
    if (empty($thissection->uservisible)) {
        return $canviewhidden or $thissection->visible or !$course->hiddensections;
    } else {
        return $thissection->uservisible ||
            ($thissection->visible && !$thissection->available && !empty($thissection->availableinfo));
    }
}

/**
 * Get a name for a section (making it up if needed)
 * @param object $course The course (as an object)
 * @param object $section The section (as an object)
 * @return string a name for the section
 */
function format_tiles_get_section_name($course, $section) {
    // We can't add a node without any text
    if ((string) $section->name !== '') {
        return format_string($section->name, true, array('context' => context_course::instance($course->id)));
    } else if ($section->section == 0) {
        return get_string('section0name', 'format_tiles');
    } else {
        return get_string('sectionname', 'format_tiles') . ' ' . $section->section;
    }
}

?>