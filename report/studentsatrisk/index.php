<?php
/**
 * Displays a report using data to help determine if a student
 * is at risk.
 * @package    report_studentsaatrisk
 * @copyright  2014 onwards Androgogic
 */

require('../../config.php');
require_once($CFG->dirroot.'/course/lib.php');
require_once($CFG->dirroot.'/report/studentsatrisk/locallib.php');
require_once($CFG->libdir.'/adminlib.php');
require_once("$CFG->libdir/completionlib.php");

global $DB, $USER;

if(session_id() == '')
{
	session_start();
}
//$id          = optional_param('id', 0, PARAM_INT);// Course ID
$pagestudent		= optional_param('pagestudent', '0', PARAM_INT);     // which page to show
$pagecourse			= optional_param('pagecourse', '0', PARAM_INT);     // which page to show
$orderstudent		= optional_param('orderstudent', '', PARAM_RAW);
$ordercourse		= optional_param('ordercourse', '', PARAM_RAW);
$directionstudent		= optional_param('directionstudent', 'asc', PARAM_RAW);
$directioncourse		= optional_param('directioncourse', 'asc', PARAM_RAW);

$pageparams = array(
	"pagestudent" => $pagestudent,
	"pagecourse" => $pagecourse,
	"orderstudent" => $orderstudent,
	"ordercourse" => $ordercourse,
	"directionstudent" => $directionstudent,
	"directioncourse" => $directioncourse
);

require_login();

$context = context_system::instance();
$PAGE->set_url('/report/studentsatrisk/index.php', $pageparams);
		$PAGE->set_pagelayout('report');
$PAGE->set_context($context).

require_capability('report/studentsatrisk:view', $context);

$toform = new stdClass();

//include linkchecker_form.php
require_once('form.php');
 
$coursechoice = array();
$coursechoice["scourse"] = array_key_exists("scourse", $_SESSION["shuffledata"]) ? $_SESSION["shuffledata"]["scourse"] : "";
$coursechoice["scount"] = array_key_exists("scourse", $_SESSION["shuffledata"]) ? count($_SESSION["shuffledata"]["scourse"]) : 0;
$coursechoice["acount"] = $DB->count_records("course");
$coursechoice["acourse"] = $DB->get_records_menu('course', array(), 'fullname', 'id, fullname');
$coursechoice["total"] = $DB->count_records("course");
 
//Instantiate linkchecker_form 
$mform = new studentsatrisk_form(null, $coursechoice);

//$mform->orderstudent = $orderstudent;
//$mform->ordercourse = $ordercourse;

$pagetype = "html";

$params = array(
	"user" => 0,
	"course" => 0,
	"accountcreatestart" => 0,
	"accountcreateend" => 0,
	"siteloginstart" => 0,
	"siteloginend" => 0,
	"lastloginstart" => 0, 
	"lastloginend" => 0, 
	"lastcourseaccessstart" => 0, 
	"lastcourseaccessend" => 0,
	"subjectviewsstart" => -1,
	"subjectviewsend" => -1,
	"forumviewsstart" => -1,
	"forumviewsend" => -1,
	"forumpostsstart" => -1,
	"forumpostsend" => -1,
	"assignmentattemptsstart" => -1,
	"assignmentattemptsend" => -1,
);


 
//Form processing and displaying is done here
if($fromform = $mform->get_data())
{
	//Shufflebox	
	if(!empty($fromform->addall))
	{
		$courses = $DB->get_records_menu('course', array(), 'fullname', 'id, fullname');
		$_SESSION["shuffledata"]["scourse"] =  $courses;
	}
	else if(!empty($fromform->addsel))
	{
        if(!empty($fromform->acourse))
        {
            if(in_array(0, $fromform->acourse))
            {
                //add_selection_all($ufiltering);
            }
            else
            {
                foreach($fromform->acourse as $courseid)
                {
                    if ($courseid == -1)
                    {
                        continue;
                    }
                    if(!isset($_SESSION["shuffledata"]["scourse"][$courseid]))
                    {
                        $_SESSION["shuffledata"]["scourse"][$courseid] = $courseid;
                    }
                }
            }
        }

    }
    else if(!empty($fromform->removeall))
    {
        $_SESSION["shuffledata"]["scourse"]= array();
    }
    else if (!empty($fromform->removesel))
    {
        if (!empty($fromform->scourse))
        {
            if (in_array(0, $fromform->scourse))
            {
                $_SESSION["shuffledata"]["scourse"]= array();
            }
            else
            {
                foreach($fromform->scourse as $courseid)
                {
                    if ($courseid == -1)
                    {
                        continue;
                    }
                    unset($_SESSION["shuffledata"]["scourse"][$courseid]);
                }
            }
        }
    }
	$_SESSION["shuffledata"]["scount"] = count($_SESSION["shuffledata"]["scourse"]);
	$_SESSION["shuffledata"]["acount"] = $DB->count_records("course");
	$_SESSION["shuffledata"]["acourse"] = $DB->get_records_menu('course', array(), 'fullname', 'id, fullname');
	$_SESSION["shuffledata"]["total"] = $DB->count_records("course");
    
    $mform = new studentsatrisk_form(null, $_SESSION["shuffledata"]);
    
    //End shufflebox
	if(array_key_exists("sarform", $_SESSION))
	{
		unset($_SESSION["sarform"]);
	}
	if(!property_exists($fromform, "clear"))
	{
		foreach($params as $key => &$value)
		{
			if(property_exists($fromform, $key))
			{
				$value =  $fromform->$key;
			}
		}
		$params["course"] = $_SESSION["shuffledata"]["scourse"];
		$_SESSION["sarform"] = $params;
		$pagetype = $fromform->pagetype;
	}
	else if(property_exists($fromform, "clear"))
	{
		$url = new moodle_url("index.php");  
		redirect($url);
	}
	
}
else if(array_key_exists("sarform", $_SESSION))
{
	$params = $_SESSION["sarform"];
}
//report_studentsatrisk_showrows


//Limits and page offsets only apply to html
$limit = ($pagetype=="html") ? $CFG->report_studentsatrisk_showrows : 0;
$pageparams["pagestudent"] = ($pagetype == "html") ? $pageparams["pagestudent"] : 0;
$pageparams["pagecourse"] = ($pagetype == "html") ? $pageparams["pagecourse"] : 0;

$results = array();
$table_headers = array();
$totalresult = array();
$table_heading = array();

//First table
$table_heading["student"] = get_string('table_student', 'report_studentsatrisk');
$table_headers["student"] = array(
	"studid" => get_string('report_studid', 'report_studentsatrisk'),
	"firstname" => get_string('report_firstname', 'report_studentsatrisk'),
	"lastname" => get_string('report_lastname', 'report_studentsatrisk'),
	"email" => get_string('report_email', 'report_studentsatrisk'),
	"timeusercreated" => get_string('report_timeusercreated', 'report_studentsatrisk'),
	"firstaccess" => get_string('report_firstaccess', 'report_studentsatrisk'),
	"lastlogin" => get_string('report_lastlogin', 'report_studentsatrisk'),
	"logincount" => get_string('report_logincount', 'report_studentsatrisk'),
	"logintime" => get_string('report_logintime', 'report_studentsatrisk'),
);

$fields = array(
	"id" => "rss.id",
	"studid" => "rss.studid",
	"firstname" => "rss.firstname",
	"lastname" => "rss.lastname",
	"email" => "rss.email",
	"timeusercreated" => "rss.timeusercreated",
	"firstaccess" => "rss.firstaccess",
	"lastlogin" => "rss.lastlogin",
	"logincount" => "rss.logincount",
	"logintime" => "rss.logintime",
);

$tables = array("{report_studentsatrisk_stud} rss");

$where = array();
$order = array(
	"rss.studid" => 1,
	"rss.firstname" => 2,
	"rss.lastname" => 3,
	"rss.email" => 4,
	"rss.timeusercreated" => 5,
	"rss.firstaccess" => 6,
	"rss.lastlogin" => 7,
	"rss.logincount" => 8,
	"rss.logintime" => 9
);

if($orderstudent != "")
{
	$orderkey = $directionstudent == "desc" ? "rss.$orderstudent DESC" : "rss.$orderstudent"; 
	$order[$orderkey] = 0;
}

$sortorder = report_studentsatrisk_build_order_array($order);


$queryparams = array();
$sqlparams = array();

if($params["user"] > 0)
{
	$where[] = "rss.userid = ?";
	$sqlparams[] = $params["user"];
}
if($params["accountcreatestart"] > 0)
{
	$where[] = "rss.timeusercreated >= ?";
	$sqlparams[] = $params["accountcreatestart"];
}
if($params["accountcreateend"] > 0)
{
	$where[] = "rss.timeusercreated <= ?";
	$sqlparams[] = $params["accountcreateend"];
}
if($params["siteloginstart"] > 0)
{
	$where[] = "rss.firstaccess >= ?";
	$sqlparams[] = $params["siteloginstart"];
}
if($params["siteloginend"] > 0)
{
	$where[] = "rss.firstaccess <= ?";
	$sqlparams[] = $params["siteloginend"];
}
if($params["lastloginstart"] > 0)
{
	$where[] = "rss.lastaccess >= ?";
	$sqlparams[] = $params["lastloginstart"];
}
if($params["lastloginend"] > 0)
{
	$where[] = "rss.lastaccess <= ?";
	$sqlparams[] = $params["lastloginend"];
}


$tablesql = join(", ", $tables);
$fieldsql = report_studentsatrisk_build_field_sql($fields);
$wheresql = (count($where) > 0) ? " WHERE " . join(" AND ", $where) : "";
$ordersql = join(", ", $sortorder);

$totalresult["student"] = $DB->count_records_sql("SELECT COUNT(*) FROM $tablesql $wheresql", $sqlparams);
$res = $DB->get_recordset_sql("SELECT $fieldsql FROM $tablesql $wheresql ORDER BY $ordersql", $sqlparams, $pageparams["pagestudent"] * $limit, $limit);

if($res->valid())
{
	//Clean up data for display as needed, and put in array as needed
	foreach($res as $id => $dat)
	{
		foreach(get_object_vars($dat) as $key => $val)
		{
			switch($key)
			{
				case "id":
					continue;
					break;
				case "timeusercreated":
				case "firstaccess":
				case "lastlogin":
					$results["student"][$id][$key] = ($val > 0) ? date("j/n/Y", $val) : "";
					break;
				case "logintime":
					$h = floor($val / 3600);
					$m = floor(($val - ($h*3600)) / 60);
					$s = floor($val % 60);
					//$results["student"][$id][$key] = "$h:$m:$s";
					$results["student"][$id][$key] = sprintf("%d:%02d:%02d", $h, $m, $s);
					break;	
				default:
					$results["student"][$id][$key] = $val;
					break;
			}
		}
	}
}
$res->close();


//Second table
$table_heading["course"] = get_string('table_course', 'report_studentsatrisk');
$table_headers["course"] = array(
	"studid" => get_string('report_studid', 'report_studentsatrisk'),
	"firstname" => get_string('report_firstname', 'report_studentsatrisk'),
	"lastname" => get_string('report_lastname', 'report_studentsatrisk'),
	"email" => get_string('report_email', 'report_studentsatrisk'),
	"code" => get_string('report_code', 'report_studentsatrisk'),
	"shortname" => get_string('report_shortname', 'report_studentsatrisk'),
	"fullname" => get_string('report_fullname', 'report_studentsatrisk'),
	"completion" => get_string('report_completion', 'report_studentsatrisk'),
	"completiondate" => get_string('report_completiondate', 'report_studentsatrisk'),
	"courseviews" => get_string('report_courseviews', 'report_studentsatrisk'),
	"firstviewed" => get_string('report_firstviewed', 'report_studentsatrisk'),
	"lastviewed" => get_string('report_lastviewed', 'report_studentsatrisk'),
	"numforumviews" => get_string('report_numforumviews', 'report_studentsatrisk'),
	"dateforumlastview" => get_string('report_dateforumlastview', 'report_studentsatrisk'),
	"numposts" => get_string('report_numposts', 'report_studentsatrisk'),
	"dateposts" => get_string('report_dateposts', 'report_studentsatrisk'),
	"timespentinforums" => get_string('report_timespentinforums', 'report_studentsatrisk'),
	"numassess" => get_string('report_numassess', 'report_studentsatrisk'),
	"datelastassess" => get_string('report_datelastassess', 'report_studentsatrisk'),
	"timespendasses" => get_string('report_timespendasses', 'report_studentsatrisk'),
);

$fields = array(
	"id" => "rsc.id",
	"studid" => "rsc.studid",
	"firstname" => "rsc.firstname",
	"lastname" => "rsc.lastname",
	"email" => "rsc.email",
	"code" => "rsc.code",
	"shortname" => "rsc.shortname",
	"fullname" => "rsc.fullname",
	"completion" => "rsc.completion",
	"completiondate" => "rsc.completiondate",
	"courseviews" => "rsc.courseviews",
	"firstviewed" => "rsc.firstviewed",
	"lastviewed" => "rsc.lastviewed",
	"numforumviews" => "rsc.numforumviews",
	"dateforumlastview" => "rsc.dateforumlastview",
	"numposts" => "rsc.numposts",
	"dateposts" => "rsc.dateposts",
	"timespentinforums" => "rsc.timespentinforums",
	"numassess" => "rsc.numassess",
	"datelastassess" => "rsc.datelastassess",
	"timespendasses" => "rsc.timespendasses",
);

$tables = array("{report_studentsatrisk_course} rsc");

$order = array(
	"rsc.studid" => 1,
	"rsc.firstname" => 2,
	"rsc.lastname" => 3,
	"rsc.email" => 4,
	"rsc.code" => 5,
	"rsc.shortname" => 6,
	"rsc.fullname" => 7,
	"rsc.completion" => 8,
	"rsc.completiondate" => 9,
	"rsc.courseviews" => 10,
	"rsc.firstviewed" => 11,
	"rsc.lastviewed" => 12,
	"rsc.numforumviews" => 13,
	"rsc.dateforumlastview" => 14,
	"rsc.numposts" => 15,
	"rsc.dateposts" => 16,
	"rsc.timespentinforums" => 17,
	"rsc.numassess" => 18,
	"rsc.datelastassess" => 19,
	"rsc.timespendasses" => 20,
);

if($ordercourse != "")
{
	$orderkey = $directioncourse == "desc" ? "rsc.$ordercourse DESC" : "rsc.$ordercourse"; 
	$order[$orderkey] = 0;
}

$sortorder = report_studentsatrisk_build_order_array($order);

$where = array();
$sqlparams = array();

if($params["user"] > 0)
{
	$where[] = "rsc.userid = ?";
	$sqlparams[] = $params["user"];
}
if(!empty($params["course"]))
{
	//$queryparams["courseid"] = $fromform->course;
	$wh = array();
	foreach($params["course"] as $c)
	{
		$wh[] = "rsc.courseid = ?";
		$sqlparams[] = $c;
	}
	$where[] = "(" . join(" OR ", $wh) . ")";
}
if($params["lastcourseaccessstart"] > 0)
{
	$where[] = "rsc.lastviewed >= ?";
	$sqlparams[] = $params["lastcourseaccessstart"];
}
if($params["lastcourseaccessend"] > 0)
{
	$where[] = "rsc.lastviewed <= ?";
	$sqlparams[] = $params["lastcourseaccessend"];
}
if($params["subjectviewsstart"] >= 0)
{
	$where[] = "rsc.courseviews >= ?";
	$sqlparams[] = $params["subjectviewsstart"];
}
if($params["subjectviewsend"] >= 0)
{
	$where[] = "rsc.courseviews <= ?";
	$sqlparams[] = $params["subjectviewsend"];
}
if($params["forumviewsstart"] >= 0)
{
	$where[] = "rsc.numforumviews >= ?";
	$sqlparams[] = $params["forumviewsstart"];
}
if($params["forumviewsend"] >= 0)
{
	$where[] = "rsc.numforumviews <= ?";
	$sqlparams[] = $params["forumviewsend"];
}
if($params["forumpostsstart"] >= 0)
{
	$where[] = "rsc.numposts >= ?";
	$sqlparams[] = $params["forumpostsstart"];
}
if($params["forumpostsend"] >= 0)
{
	$where[] = "rsc.numposts <= ?";
	$sqlparams[] = $params["forumpostsend"];
}
if($params["assignmentattemptsstart"] >= 0)
{
	$where[] = "rsc.numassess >= ?";
	$sqlparams[] = $params["assignmentattemptsstart"];
}
if($params["assignmentattemptsend"] >= 0)
{
	$where[] = "rsc.numassess <= ?";
	$sqlparams[] = $params["assignmentattemptsend"];
}



$tablesql = join(", ", $tables);
$fieldsql = report_studentsatrisk_build_field_sql($fields);
$wheresql = (count($where) > 0) ? " WHERE " . join(" AND ", $where) : "";
$ordersql = join(", ", $sortorder);

$totalresult["course"] = $DB->count_records_sql("SELECT COUNT(*) FROM $tablesql $wheresql", $sqlparams);
$res = $DB->get_recordset_sql("SELECT $fieldsql FROM $tablesql $wheresql ORDER BY $ordersql ", $sqlparams, $pageparams["pagecourse"] * $limit, $limit);


if($res->valid())
{
	//Clean up data for display as needed, and put in array as needed
	foreach($res as $id => $dat)
	{
		foreach(get_object_vars($dat) as $key => $val)
		{
			switch($key)
			{
				case "id":
					continue;
					break;
				case "completiondate":
				case "firstviewed":
				case "lastviewed":
				case "dateforumlastview":
				case "dateposts":
				case "datelastassess":
					$results["course"][$id][$key] = ($val > 0) ? date("j/n/Y", $val) : "";
					break;
				case "timespentinforums":
				case "timespendasses":
					$h = floor($val / 3600);
					$m = floor(($val - ($h*3600)) / 60);
					$s = floor($val % 60);
					$results["course"][$id][$key] = sprintf("%d:%02d:%02d", $h, $m, $s);
					break;	
				default:
					$results["course"][$id][$key] = $val;
					break;
			}
		}
	}
}
$res->close();

switch($pagetype)
{
	case "html":
	default:
		
		$PAGE->set_title("Student retention report");
        $PAGE->set_heading("Student retention report");
        $PAGE->navbar->add("Student retention report");
        echo $OUTPUT->header();
        echo $OUTPUT->heading("Student retention report");
        $params["orderstudent"] = $orderstudent;
        $params["ordercourse"] = $ordercourse;
        $mform->set_data($params);
		//displays the form
		$mform->display();
        
        report_studentsatrisk_print_html($table_heading, $table_headers, $results, $pageparams, $totalresult);
		
	break;
	case 'csv':
		report_studentsatrisk_print_csv($table_heading, $table_headers, $results);
		exit;
	break;
	case 'xls':
		report_studentsatrisk_print_xls($table_heading, $table_headers, $results);
		exit;
	break;
}
echo $OUTPUT->footer();

