<footer id="page-footer" class="row-fluid">
    <div id="footnotewrapper" class="container-fluid">
        <div class="year">&copy;
        <?php echo date('Y'); ?>
        </div>
        <?php echo $html->footnote; ?>
        <div class="helplink"><?php echo $OUTPUT->page_doc_link(); ?></div>
    </div> 
</footer>