<footer id="page-footer" class="row-fluid">
    <div id="footnotewrapper" class="container-fluid">
        <div class="logo2">
            <img src="<?php echo $OUTPUT->pix_url('logo2', 'theme')?>" alt="Smarter Choice Learning" />
        </div>
        <div class="footer-text">&copy;
            <?php echo date('Y'); ?>
            Smarter Choice. All Rights Reserved. | <a href="#">Terms & Conditions</a>
            <br/>
            <?php echo $OUTPUT->page_doc_link(); ?>
        </div>
        <!--<?php echo $html->footnote; ?>-->
        <div class="footer-logos">
            <img src="<?php echo $OUTPUT->pix_url('logo_footer', 'theme')?>" alt="" />
        </div>
        
    </div> 
</footer>