<?php

defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {

        // Logo file setting.
        $name = 'theme_oehsc/logo';
        $title = get_string('logo','theme_oehsc');
        $description = get_string('logodesc', 'theme_oehsc');
        //$default = '';
        $setting = new admin_setting_configstoredfile($name, $title, $description, 'logo');
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);
        
        // Favicon file setting.
        $name = 'theme_oehsc/favicon';
        $title = new lang_string('favicon', 'theme_oehsc');
        $description = new lang_string('favicondesc', 'theme_oehsc');
        $setting = new admin_setting_configstoredfile($name, $title, $description, 'favicon', 0, array('accepted_types' => '.ico'));
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);

        //Custom CSS file
        $name = 'theme_oehsc/customcss';
        $title = get_string('customcss', 'theme_oehsc');
        $description = get_string('customcssdesc', 'theme_oehsc');
        $default = '';
        $setting = new admin_setting_configtextarea($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);

        //Footnote setting
        $name = 'theme_oehsc/footnote';
        $title = get_string('footnote', 'theme_oehsc');
        $description = get_string('footnotedesc', 'theme_oehsc');
        $default = '';
        $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);
}