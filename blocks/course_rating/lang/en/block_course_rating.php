<?php 

$string['pluginname']				= 'Course rating';
$string['plugintitle']				= 'Course rating';
$string['noratings']				= 'Not yet rated';
$string['myrating']					= 'My rating:';
$string['avgrating']				= 'Course rating (all):';
$string['course_rating:addinstance'] = 'Add a new course rating block';
$string['course_rating:rate'] = 'Rate the course';
// End of blocks/course_rating/lang/en/block_course_rating.php
