<?php
/**
 * Androgogic Moodle Course Ratings
 * 
 * @author Eamon Delaney   
 * @version v1.0   
 * @copyright Eamon Delaney
 *
 **/
require_once($CFG->dirroot . '/blocks/course_rating/lib.php');

class block_course_rating extends block_base {

    function init() {

        global $CFG;

		$this->title = get_string('pluginname', 'block_course_rating');

	}

	function applicable_formats() {
		return array('all' => TRUE);
	}

	function instance_can_be_hidden() {
		return TRUE;
	}

	/*function has_config() {
		return TRUE;
	}*/

	function get_content() {
        global $CFG, $USER, $COURSE, $PAGE, $DB, $OUTPUT;
        
		//Check if the content has already been generated
        if($this->contentgenerated === TRUE)
        {
            return $this->content;
        }
        
        $this->page->requires->js('/blocks/course_rating/rating.js');
        
        $courseid = $COURSE->id;
		//Block output. Stars for rating
        $this->content = new stdClass;
        
        $my_rating_record = $DB->get_record("block_course_rating", array("courseid" => $courseid, "userid" => $USER->id));
        $my_rating = 0;
        $attr_array = array("class"=>"myrate_label");
        //If user has rated befoe
        if(!empty($my_rating_record))
        {
			$my_rating = $my_rating_record->rating;
			$attr_array["class"] = "myrate_label myrate_label_strong";
		}
		
		 $this->content->text = html_writer::tag('p', get_string('myrating', 'block_course_rating'), $attr_array);
		
		//0 stars
		$url = new moodle_url("/blocks/course_rating/rate.php", array("rating" => 0, "courseid"=>$courseid));
		$img = html_writer::tag('div', '', array("class" => "rating_star starnull"));
		$this->content->text .= html_writer::link($url, $img, array("class"=>"ratelink"));
        
		//Make a rating
		for($i=1; $i<=5; $i++)
		{
			$url = new moodle_url("/blocks/course_rating/rate.php", array("rating" => $i, "courseid"=>$courseid));
			//Prefill the stars if they have previously rated the course
			$class = ($i <= $my_rating ) ? "rating_star starfull starstart" : "rating_star";
			$img = html_writer::tag('div', '', array("class" => $class, "id" => 'ind-'.($i)));
			$this->content->text .= html_writer::link($url, $img, array("class"=>"ratelink"));
			//$this->content->text .= html_writer::tag('span', $i, array("class" => "ratinglabel"));
		}
		
		//Numeric labels
		for($i=0; $i<=5; $i++)
		{
			$ratelabels .= html_writer::tag('div', $i);
		}
		
		$this->content->text .= html_writer::tag('div', $ratelabels, array("class" => "ratinglabel"));
        
        //Current rating
        $total_ratings = get_count_rating($courseid);
        
        //If no ratings display nothing, else display average rating
        if($total_ratings == 0)
        {
			$this->content->text .=  html_writer::tag('p', get_string('noratings', 'block_course_rating'), array("class" => "no_label"));
			
			$this->content->text .= html_writer::tag('p', get_string('avgrating', 'block_course_rating'), array("class" => "avg_label starhide"));
			for($i=1; $i<=5; $i++)
			{	
				//Prefill the stars if they have previously rated the course
				$class = "average_star starhide";
				$this->content->text .= html_writer::tag('div', '', array("class" => $class)); 
			}
			$this->content->text.= html_writer::tag("span", "($total_ratings)", array("class" => "total_rating starhide"));
		}
		else
		{
			$this->content->text .= html_writer::tag('p', get_string('avgrating', 'block_course_rating'), array("class" => "avg_label"));
			
			$avg = get_average_rating($courseid);
			for($i=1; $i<=5; $i++)
			{	
				//Prefill the stars if they have previously rated the course
				$class = ($i <= $avg ) ? "average_star starfull" : "average_star";
				$this->content->text .= html_writer::tag('div', '', array("class" => $class)); 
			}
			$this->content->text.= html_writer::tag("span", "($total_ratings)", array("class" => "total_rating"));
		}
        

        $this->content->footer = "";

        $this->contentgenerated = TRUE;
        return $this->content;

	}
}
// End of blocks/course_rating/block_course_rating.php
