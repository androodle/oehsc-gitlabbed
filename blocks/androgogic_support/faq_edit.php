<?php

/** 
 * Androgogic Support Block: Edit object
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     06/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Edit one of the faqs
 *
 **/

global $OUTPUT;

require_capability('block/androgogic_support:edit', $context);

require_once('faq_edit_form.php');
$id = required_param('id', PARAM_INT);
$q = "select DISTINCT a.*  
from mdl_androgogic_faq a 
where a.id = $id ";
$faq = $DB->get_record_sql($q);
$mform = new faq_edit_form();
if ($data = $mform->get_data()){
$data->id = $id;
$data->modified_by = $USER->id;
$data->date_modified = date('Y-m-d H:i:s');
$data->question = format_text($data->question['text'], $data->question['format']);
$data->answer = format_text($data->answer['text'], $data->answer['format']);
$DB->update_record('androgogic_faq',$data);
echo $OUTPUT->notification(get_string('datasubmitted','block_androgogic_support'), 'notifysuccess');
}
else{
echo $OUTPUT->heading(get_string('faq_edit', 'block_androgogic_support'));
$mform->display();
}

?>
